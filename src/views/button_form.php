<?php

use junati\modalwidget\ModalWidgetAsset;

ModalWidgetAsset::register($this);

?>

<?php if ($options['deletePermission']) { ?>
    <button type="button" class="<?= $options['buttonClass'] ?>"
            onclick="buttonFunction(this, '<?= $options['form_path'] ?>', '<?= $options['listClass'] ?>', '<?= $options['page_reload'] ?>','<?= $options['message'] ?>')">
        <?= $options['name'] ?>
    </button>
<?php } ?>